using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------------------------------------------//
//                                                //
//------------------------------------------------//

public static class ExtensionMethods {

	//------------------------------------------------//
	// String                                         //
	//------------------------------------------------//

	//------------------------------------------------//
	public static string FormatAbsoluteURL (this string s, string urlBase) {
		if (!System.Uri.IsWellFormedUriString (s, System.UriKind.Absolute)) {
			return string.Format ("{0}{1}", urlBase, s);
		}
		else {
			return s;
		}
	}

	//------------------------------------------------//
	public static string ToUpperFirstLetter (this string s) {
		return string.Concat (s[0].ToString ().ToUpper (), s.Substring (1));
	}

	//------------------------------------------------//
	// Transform                                      //
	//------------------------------------------------//

	//------------------------------------------------//
	public static Transform Clear (this Transform transform) {
		foreach (Transform child in transform) {
			GameObject.Destroy (child.gameObject);
		}
		return transform;
	}
}
